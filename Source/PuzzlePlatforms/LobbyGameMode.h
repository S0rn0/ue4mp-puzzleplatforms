// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PuzzlePlatformsGameMode.h"
#include "LobbyGameMode.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API ALobbyGameMode final : public APuzzlePlatformsGameMode
{
	GENERATED_BODY()

public:
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

private:

	// TODO-DF Probably better to just get all player controllers and get the length from that. Look into this.
	uint8 NumPlayerInLobby = 0;

	FTimerHandle GameStartTimerHandle;

	void StartGame();
};

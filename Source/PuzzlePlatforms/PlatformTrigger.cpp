// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PlatformTrigger.h"

#include "MovingPlatform.h"

#include "Components/BoxComponent.h"

APlatformTrigger::APlatformTrigger()
{
	PrimaryActorTick.bCanEverTick = true;

  TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("Trigger Volume"));
  if (!ensure(TriggerVolume != nullptr)) return;

  TriggerVolume->SetupAttachment(RootComponent);
}

void APlatformTrigger::BeginPlay()
{
	Super::BeginPlay();

  if (!ensure(TriggerVolume != nullptr)) return;
  TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &APlatformTrigger::OnComponentBeginOverlap);
  TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &APlatformTrigger::OnComponentEndOverlap);
}

void APlatformTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlatformTrigger::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
  for (AMovingPlatform* MovingPlatform : PlatformsToTrigger)
  {
    MovingPlatform->AddActiveTrigger();
  }
}


void APlatformTrigger::OnComponentEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) 
{
  for (AMovingPlatform* MovingPlatform : PlatformsToTrigger)
  {
    MovingPlatform->RemoveActiveTrigger();
  }
}



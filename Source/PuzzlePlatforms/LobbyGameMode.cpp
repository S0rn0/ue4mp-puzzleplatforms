// Copyright © 2021 Derek Fletcher, All rights reserved

#include "LobbyGameMode.h"

#include "PuzzlePlatformsGameInstance.h"

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	
	if (++NumPlayerInLobby == UPuzzlePlatformsGameInstance::MaxPlayers)
	{
		GetWorldTimerManager().SetTimer(GameStartTimerHandle, this, &ALobbyGameMode::StartGame, 5.0f);
	}
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	
	if (--NumPlayerInLobby < UPuzzlePlatformsGameInstance::MaxPlayers)
	{
		GetWorldTimerManager().ClearTimer(GameStartTimerHandle);
	}
}

void ALobbyGameMode::StartGame()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	UPuzzlePlatformsGameInstance* GameInstance = Cast<UPuzzlePlatformsGameInstance>(GetGameInstance());

	if (GameInstance != nullptr)
	{
		GameInstance->StartSession();
		
		bUseSeamlessTravel = true;
		World->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap");
	}
}

// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "PlatformTrigger.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API APlatformTrigger final : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	APlatformTrigger();
	virtual void Tick(float DeltaTime) override;

private:

	virtual void BeginPlay() override;

  UPROPERTY(EditAnywhere)
  class UBoxComponent* TriggerVolume;

  UPROPERTY(EditAnywhere)
  TArray<class AMovingPlatform*> PlatformsToTrigger;

  UFUNCTION()
  void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
  void OnComponentEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};

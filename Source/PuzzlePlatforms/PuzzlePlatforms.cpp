// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PuzzlePlatforms.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PuzzlePlatforms, "PuzzlePlatforms" );
 
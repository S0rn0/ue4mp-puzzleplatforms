// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PuzzlePlatformsGameInstance.h"

#include "UObject/ConstructorHelpers.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"

const static FName SESSION_NAME = "My Session Game";

void UPuzzlePlatformsGameInstance::Init() 
{
  IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get();
  if (!ensure(OnlineSubsystem != nullptr)) return;

  SessionInterface = OnlineSubsystem->GetSessionInterface();
  if (SessionInterface.IsValid()) {
      SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnCreateSessionComplete);
      SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnDestroySessionComplete);
      SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnFindSessionsComplete);
      SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnJoinSessionComplete);
  }

  if (GEngine != nullptr)
  {
    GEngine->NetworkFailureEvent.AddUObject(this, &UPuzzlePlatformsGameInstance::OnNetworkFailure);
  }
}

void UPuzzlePlatformsGameInstance::Host() const
{
  if (SessionInterface.IsValid()) {
    FNamedOnlineSession* ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);

    if (ExistingSession != nullptr)
    {
      SessionInterface->DestroySession(SESSION_NAME);
    }
    else
    {
      CreateSession();
    }
  }
}

void UPuzzlePlatformsGameInstance::Join() 
{
  SessionSearch = MakeShareable(new FOnlineSessionSearch());
  if (SessionSearch.IsValid())
  {
    SessionSearch->bIsLanQuery = true;
    SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
  }
}

void UPuzzlePlatformsGameInstance::StartSession() const
{
  if (SessionSearch.IsValid())
  {
    SessionInterface->StartSession(SESSION_NAME);
  }
}

void UPuzzlePlatformsGameInstance::CreateSession() const
{
  if (SessionInterface.IsValid())
  {
    FOnlineSessionSettings SessionSettings;
    SessionSettings.bIsLANMatch = true;
    SessionSettings.NumPublicConnections = MaxPlayers;
    SessionSettings.bShouldAdvertise = true;

    SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
  }
}

void UPuzzlePlatformsGameInstance::OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
  APlayerController* PlayerController = GetFirstLocalPlayerController();
  if (!ensure(PlayerController != nullptr)) return;

  PlayerController->ClientTravel("/Game/Maps/Lobby", ETravelType::TRAVEL_Absolute);
}

void UPuzzlePlatformsGameInstance::OnCreateSessionComplete(const FName SessionName, const bool bWasSuccessful) const
{
  if (!bWasSuccessful) return;

  UWorld* World = GetWorld();
  if (!ensure(World != nullptr)) return;

  World->ServerTravel("/Game/Maps/Lobby?listen");
}

void UPuzzlePlatformsGameInstance::OnDestroySessionComplete(const FName SessionName, const bool bWasSuccessful) const
{
  if (bWasSuccessful)
  {
    CreateSession();
  }
}

void UPuzzlePlatformsGameInstance::OnFindSessionsComplete(const bool bWasSuccessful) const
{
  if (bWasSuccessful && SessionInterface.IsValid() && SessionSearch.IsValid() && SessionSearch->SearchResults.Num() > 0)
  {
    SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[0]);
  }
}

void UPuzzlePlatformsGameInstance::OnJoinSessionComplete(const FName SessionName, const EOnJoinSessionCompleteResult::Type Result) const
{
  if (SessionInterface.IsValid())
  {
    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!ensure(PlayerController != nullptr)) return;

    FString Address;
    if (!SessionInterface->GetResolvedConnectString(SessionName, Address)) return;

    PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
  }
}

// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "PuzzlePlatformsGameInstance.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API UPuzzlePlatformsGameInstance final : public UGameInstance
{
	GENERATED_BODY()

public:

  static const uint8 MaxPlayers = 2;

  virtual void Init() override;

  UFUNCTION(Exec)
  void Host() const;

  UFUNCTION(Exec)
  void Join();

  void StartSession() const;
  
private:

  IOnlineSessionPtr SessionInterface;
  TSharedPtr<class FOnlineSessionSearch> SessionSearch;

  void CreateSession() const;

  void OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);
  
  void OnCreateSessionComplete(const FName SessionName, const bool bWasSuccessful) const;
  void OnDestroySessionComplete(const FName SessionName, const bool bWasSuccessful) const;
  void OnFindSessionsComplete(const bool bWasSuccessful) const;
  void OnJoinSessionComplete(const FName SessionName, const EOnJoinSessionCompleteResult::Type Result) const;
};


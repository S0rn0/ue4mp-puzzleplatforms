// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API AMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:

  AMovingPlatform();
  virtual void Tick(float DeltaTime) override;

  void AddActiveTrigger();
  void RemoveActiveTrigger();

private:

  UPROPERTY(EditAnywhere)
  float Speed = 0.f;

  UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
  FVector TargetLocation;

  UPROPERTY(EditAnywhere)
  int ActivatedTriggers = 1;

  virtual void BeginPlay() override;

  FVector StartingLocation;
  FVector FinishingLocation;

};

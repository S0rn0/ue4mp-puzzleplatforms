// Copyright © 2021 Derek Fletcher, All rights reserved

#include "MovingPlatform.h"

AMovingPlatform::AMovingPlatform() 
{
  PrimaryActorTick.bCanEverTick = true;
  SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::BeginPlay() 
{
  Super::BeginPlay();

  StartingLocation = GetActorLocation();
  FinishingLocation = GetTransform().TransformPosition(TargetLocation);

  if (HasAuthority())
  {
    SetReplicates(true);
    SetReplicateMovement(true);
  }
}

void AMovingPlatform::Tick(float DeltaTime) 
{
  Super::Tick(DeltaTime);

  if (ActivatedTriggers > 0 && HasAuthority())
  {
    const float JourneyLength = (FinishingLocation - StartingLocation).Size();
    const float JourneyTravelled = (GetActorLocation() - StartingLocation).Size();

    if (JourneyTravelled >= JourneyLength)
    {
      const FVector Swap = StartingLocation;
      StartingLocation = FinishingLocation;
      FinishingLocation = Swap;
    }

    const FVector Direction = (FinishingLocation - StartingLocation).GetSafeNormal();
    AddActorWorldOffset(Direction * Speed * DeltaTime);
  }
}

void AMovingPlatform::RemoveActiveTrigger() 
{
  ActivatedTriggers--;
}

void AMovingPlatform::AddActiveTrigger() 
{
  ActivatedTriggers++;
}

